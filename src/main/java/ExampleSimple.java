import java.io.IOException;
import java.net.InetAddress;
import java.util.NoSuchElementException;

import net.tomp2p.dht.FutureGet;
import net.tomp2p.dht.PeerBuilderDHT;
import net.tomp2p.dht.PeerDHT;
import net.tomp2p.futures.FutureBootstrap;
import net.tomp2p.p2p.PeerBuilder;
import net.tomp2p.peers.Number160;
import net.tomp2p.storage.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExampleSimple {

    final private PeerDHT peer;
    Logger log = LoggerFactory.getLogger(ExampleSimple.class);

    public ExampleSimple(int peerId) throws Exception {
        log.info("Creating peer with ID: {}", peerId);
        peer = new PeerBuilderDHT(new PeerBuilder(Number160.createHash(peerId)).ports(4000 + peerId).start()).start();
        final int bootstrapPort= 4020;

        log.info("Connecting to bootstrab server on port: {}", bootstrapPort);
        FutureBootstrap futureBootstrap = this.peer.peer().bootstrap().inetAddress(InetAddress.getByName("127.0.0.1")).ports(bootstrapPort).start();
        futureBootstrap.awaitUninterruptibly();

        if (futureBootstrap.isSuccess()) {
            peer.peer().discover().peerAddress(futureBootstrap.bootstrapTo().iterator().next()).start().awaitUninterruptibly();
            log.info("Bootstrap successful");
        } else {
            log.error(futureBootstrap.failedReason());
            System.exit(-1);
        }
    }

    public static void main(String[] args) throws NumberFormatException, Exception {
        ExampleSimple dns = new ExampleSimple(Integer.parseInt(args[0]));
        if (args.length == 3) {
            dns.store(args[1], args[2]);
        }
        if (args.length == 2) {
            System.out.println("Getting KV");
            System.out.println("Name:" + args[1] + " IP:" + dns.get(args[1]));
        }
    }

    private String get(String name) throws ClassNotFoundException, IOException {
        log.info("Getting for key: {}", name);
        try {
            FutureGet futureGet = peer.get(Number160.createHash(name)).start();
            futureGet.awaitUninterruptibly();
            if (futureGet.isSuccess()) {
                return futureGet.dataMap().values().iterator().next().object().toString();
            }
        } catch (NoSuchElementException e) {
            log.info("No Such element exists!");
        }
        return "not found";

    }

    private void store(String name, String ip) throws IOException {
        log.info("Storing {}: {}", name, ip);
        peer.put(Number160.createHash(name)).data(new Data(ip)).start().awaitUninterruptibly();
    }
}
